<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    
    Route::get('persons', 'UserController@index')->name('persons');
    Route::get('persons/ajax/createForm', 'UserController@createForm');
    Route::get('persons/ajax/getDepartmentsByCountry', 'UserController@getDepartmentsByCountry');
    Route::get('persons/ajax/getMunicipalitiesByDepartment', 'UserController@getMunicipalitiesByDepartment');
    Route::post('persons/ajax/create', 'UserController@create');
    Route::get('persons/ajax/updateForm', 'UserController@updateForm');
    Route::get('persons/ajax/search', 'UserController@search');
    Route::get('persons/ajax/view', 'UserController@view');
    Route::post('persons/ajax/update', 'UserController@update');
    Route::post('persons/ajax/delete', 'UserController@delete');
    Route::get('persons/ajax/changePassword', 'UserController@changePassword');
    Route::post('persons/ajax/savePass', 'UserController@savePass');

    Route::get('login', 'Auth\AuthController@login')->name('login');
    Route::post('login', 'Auth\AuthController@authenticate');
    
});

Route::group(['middleware' => ['web', 'auth']], function () {

    Route::get('/', 'HomeController@index');
    Route::get('home', 'HomeController@index');
    Route::get('logout', 'Auth\AuthController@logout');

    Route::get('emails', 'EmailController@index');
    Route::get('emails/createForm', 'EmailController@createForm');
    Route::post('emails/create', 'EmailController@create');
});
