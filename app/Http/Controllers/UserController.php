<?php

namespace App\Http\Controllers;

use App\User;
use App\Person;
use App\Country;
use App\DocIdType;
use Carbon\Carbon;
use App\Department;
use App\PhoneTypes;
use App\CivilStatus;
use App\Municipality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Vista principal para la gestión de personas
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request) {

        return view('users.index');
    }

    /**
     * Formulario de creación de personas
     *
     * @param Request $request
     * @return void
     */
    public function createForm(Request $request) {

        if ($request->ajax()) {

            //Tipo de documento de identidad
            $doc_id_types = DocIdType::orderBy('name', 'asc')
                ->get();

            //Estado civíl
            $civil_statuses = CivilStatus::all();
            
            //Tipo de teléfono
            $phone_types = PhoneTypes::all();

            //Carga los países activos, para efectos de demostración el único
            //país activo es Colombia
            $countries = Country::where('active', 1)
                ->orderBy('name', 'asc')
                ->get();
            
            return response()->json(view('users.create', compact('doc_id_types', 'civil_statuses', 'phone_types', 'countries'))->render());
        }
    }

    /**
     * Función de busqueda de personas
     *
     * @param Request $request
     * @return void
     */
    public function search(Request $request) {

        if ($request->ajax()) {

            //Filtros para la busqueda
            $filter = [];

            if (!empty($request->f_username)) {
                $filter[] = ["u.name", "LIKE", "%".$request->f_username."%"];
            }
            if (!empty($request->f_email)) {
                $filter[] = ["u.email", "LIKE", "%".$request->f_email."%"];
            }
            if (!empty($request->f_first_name)) {
                $filter[] = ["p.first_name", "LIKE", "%".$request->f_first_name."%"];
            }
            if (!empty($request->f_last_name)) {
                $filter[] = ["p.last_name", "LIKE", "%".$request->f_last_name."%"];
            }
            
            $persons = DB::table('people AS p')
                ->join('users AS u', 'u.id', '=', 'p.user_id')
                ->join('doc_id_types AS dit', 'dit.id', '=', 'p.doc_id_type_id')
                ->join('civil_statuses AS ce', 'ce.id', '=', 'p.civil_status_id')
                ->join('phone_types AS pt', 'pt.id', '=', 'p.phone_type_id')
                ->where($filter)
                ->select("u.name AS username", "u.id AS user_id", "u.email", "p.id", "p.first_name", "p.last_name", "dit.name AS doc_id_type", "p.doc_id", "p.birthday", "p.gender", "ce.name AS civil_status", "pt.name AS phone_type", "p.phone", "p.address", "p.created_at")
                ->orderBy('p.created_at', 'DESC')
                ->paginate(10);

            //Convierto la fecha de formato yyyy-mm-dd a dd/mm/yyyy
            foreach ((object)$persons as $person) {
                if ($person->birthday) {
                    $person->birthday = Carbon::parse($person->birthday)->format('d/m/Y');
                }
            }

            //Hago la misma conversión de fecha, pero adicionalmente agrego hora, minitos y segundos
            foreach ((object)$persons as $person) {
                if ($person->created_at) {
                    $person->created_at = Carbon::parse($person->created_at)->format('d/m/Y h:i:s a');
                }
            }

            return response()->json(view('users.search', compact('persons'))->render());
        }
    }

    /**
     * Función para cargar los departamentos por país
     *
     * @param Request $request
     * @return void
     */
    public function getDepartmentsByCountry(Request $request) {
        
        if ($request->ajax()) {

            $this->validate($request, [
                'countryId' => 'required|exists:countries,id'
            ]);

            $departments = Department::where('country_id', $request->countryId)
                ->orderBy('name', 'asc')
                ->get();

            return response()->json(view('selects.departments', compact('departments'))->render());
        }
    }

    /**
     * Función para cargar los municipios por departamento
     *
     * @param Request $request
     * @return void
     */
    public function getMunicipalitiesByDepartment(Request $request) {
        
        if ($request->ajax()) {

            $this->validate($request, [
                'departmentId' => 'required|exists:departments,id'
            ]);

            $municipalities = Municipality::where('department_id', $request->departmentId)
                ->orderBy('name', 'asc')
                ->get();

            return response()->json(view('selects.municipalities', compact('municipalities'))->render());
        }
    }

    /**
     * Muestra un registro de una persona en modo solo lectura
     *
     * @param Request $request
     * @return void
     */
    public function view(Request $request) {

        if ($request->ajax()) {
            
            $this->validate($request, [
                'personId' => 'required|exists:people,id'
            ]);

            $person = DB::table('people AS p')
                ->join('users AS u', 'u.id', '=', 'p.user_id')
                ->where('p.id', $request->personId)
                ->select("p.*", "u.name AS username", "u.email")
                ->first();

            $doc_id_types = DocIdType::where('id', $person->doc_id_type_id)
                ->first();

            $civil_statuses = CivilStatus::where('id', $person->civil_status_id)
                ->first();

            $phone_types = PhoneTypes::where('id', $person->phone_type_id)
                ->first();

            $department = null;
            $municipality = null;
            $country = null;

            if (!empty($person->municipality_id)) {
                
                $geo = DB::table('countries AS c')
                    ->join('departments AS d', 'd.country_id', '=', 'c.id')
                    ->join('municipalities AS m', function($join) use ($person) {
                        $join->on('m.department_id', '=', 'd.id')
                            ->where('m.id', $person->municipality_id);
                    })
                    ->select('c.id AS country_id', 'd.id AS department_id')
                    ->first();

                $department = Department::where('id', $geo->department_id)
                    ->first();

                $municipality = Municipality::where('id', $person->municipality_id)
                    ->first();

                $country = Country::where('id', $geo->country_id)
                    ->first();
            }

            return response()->json(view('users.view', compact('person', 'doc_id_types', 'civil_statuses', 'phone_types', 'country', 'department', 'municipality'))->render());
        }
    }

    /**
     * Formulario que carga los datos de una persona para ser editados
     *
     * @param Request $request
     * @return void
     */
    public function updateForm(Request $request) {
        if ($request->ajax()) {
            $this->validate($request, [
                'personId' => 'required|exists:people,id,active,1',
            ]);

            $person = DB::table('people AS p')
                ->join('users AS u', 'u.id', '=', 'p.user_id')
                ->where('p.id', $request->personId)
                ->select("p.*", "u.name AS username", "u.email")
                ->first();

            $doc_id_types = DocIdType::selectRaw("id, name, (CASE WHEN id = ? THEN 'selected' ELSE '' END) AS selected", [$person->doc_id_type_id])
                ->orderBy('name', 'asc')
                ->get();

            $civil_statuses = CivilStatus::selectRaw("id, name, (CASE WHEN id = ? THEN 'selected' ELSE '' END) AS selected", [$person->civil_status_id])
                ->get();
            $phone_types = PhoneTypes::selectRaw("id, name, (CASE WHEN id = ? THEN 'selected' ELSE '' END) AS selected", [$person->phone_type_id])
                ->get();

            $departments = null;
            $municipalities = null;
            if (!empty($person->municipality_id)) {
                
                $geo = DB::table('countries AS c')
                    ->join('departments AS d', 'd.country_id', '=', 'c.id')
                    ->join('municipalities AS m', function($join) use ($person) {
                        $join->on('m.department_id', '=', 'd.id')
                            ->where('m.id', $person->municipality_id);
                    })
                    ->select('c.id AS country_id', 'd.id AS department_id')
                    ->first();

                $departments = Department::selectRaw("id, name, (CASE WHEN id = ? THEN 'selected' ELSE '' END) AS selected", [!empty($geo->department_id)?$geo->department_id:0])
                    ->orderBy('name', 'asc')
                    ->get();

                $municipalities = Municipality::selectRaw("id, name, (CASE WHEN id = ? THEN 'selected' ELSE '' END) AS selected", [!empty($person->municipality_id)?$person->municipality_id:0])
                    ->orderBy('name', 'asc')
                    ->get();
            }

            $countries = Country::selectRaw("id, name, (CASE WHEN id = ? THEN 'selected' ELSE '' END) AS selected", [!empty($geo->country_id)?$geo->country_id:0])
                ->where('active', 1)
                ->orderBy('name', 'asc')
                ->get();

            //$saved es un flag que me permite conocer cuando si una persona
            //ha sido creada anterior a esta acción para mostrar una notificación
            $saved = false;
            if ($request->has('saved')) {
                $saved = $request->saved;
            }
            
            return response()->json(view('users.create', compact('person', 'doc_id_types', 'civil_statuses', 'phone_types', 'countries', 'departments', 'municipalities', 'saved'))->render());
            
        }
    }

    /**
     * Función que elimina una persona y su usuario de la bd
     *
     * @param Request $request
     * @return void
     */
    public function delete(Request $request) {
        if ($request->ajax()) {
            $this->validate($request, [
                'personId' => 'required|exists:people,id'
            ]);

            $person = Person::where('id', $request->personId)
                ->first();
                
            $user = User::where('id', $person->user_id)
                ->first();

            DB::beginTransaction();
            $person->delete();
            $user->delete();

            DB::commit();

            return response()->json([
                'success' => '1'
            ]);
        }
    }

    /**
     * Función que guarda los datos editados de una persona
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request) {

        if ($request->ajax()) {
            
            //Validaciones de campos
            $this->validate($request, [
                'user_id' => 'required|exists:users,id',
                'username' => 'required',
                'email' => 'required|email|unique:users,email,'.$request->user_id,
                'name' => 'required',
                'last_name' => 'required',
                'doc_id_type' => 'required|exists:doc_id_types,id',
                'doc_id' => 'required',
                'birthday' => 'required|date|before:tomorrow',
                'gender' => 'required',
                'civil_status' => 'required|exists:civil_statuses,id',
                'phone_type' => 'required|exists:phone_types,id',
                'phone' => 'required|max:13',
                'country' => 'required|exists:countries,id',
                'department' => 'required|exists:departments,id',
                'municipality' => 'required|exists:municipalities,id',
                'address' => 'required'
            ],[
                'birthday.before' => __('messages.date_before_tomorrow')
            ]);

            //Se actualiza los datos del usuario
            $user = User::where('id', $request->user_id)
                ->first();
            $user->name = $request->username;
            $user->email = $request->email;

            DB::beginTransaction();
            $user->save();

            //Se actualiza los datos de la persona
            $person = Person::where('user_id', $user->id)
                ->first();
            $person->first_name = $request->name;
            $person->last_name = $request->last_name;
            $person->doc_id_type_id = $request->doc_id_type;
            $person->doc_id = $request->doc_id;
            $person->birthday = $request->birthday;
            $person->gender = $request->gender;
            $person->civil_status_id = $request->civil_status;
            $person->address = $request->address;
            $person->phone_type_id = $request->phone_type;
            $person->phone = $request->phone;

            if (!empty($request->municipality)) {
                $person->municipality_id = $request->municipality;
            }
            $person->save();
            
            DB::commit();

            return response()->json([
                'person' => $person,
            ]);
        }
    }

    /**
     * Actualiza la contraseña de un usuario
     *
     * @param Request $request
     * @return void
     */
    public function savePass(Request $request) {
        if ($request->ajax()) {
            $this->validate($request, [
                'user_id' => 'required|exists:users,id',
                'current_pass' => 'required|min:6',
                'new_pass' => 'required|min:6|different:current_pass',
                'password_confirmation' => 'required|same:new_pass'
            ],[
                'password_confirmation.same' => __('messages.pass_confirmation_same'),
            ]);

            $user = User::where('id', $request->user_id)
                ->first();

            //Verifico si la contraseña actual ingresada es igual a la registrada
            //en la bd
            if (Hash::check($request->current_pass, $user->password)) {
                $user->password = Hash::make($request->new_pass);
                $user->save();

                return response()->json([
                    'success' => '1'
                ]);

            } else {
                
                return response()->json([
                    "message" => "",
                    "errors" => ["current_pass" => [__("messages.pass_incorrect")]],
                ], 422);
            }
        }
    }

    /**
     * Muestra el formulario para cambiar la contraseña de un usuario
     *
     * @param Request $request
     * @return void
     */
    public function changePassword(Request $request) {

        if ($request->ajax()) {
            $this->validate($request, [
                'userId' => 'required|exists:users,id'
            ]);

            $person = Person::where('user_id', $request->userId)
                ->first();

            return response()->json(view('users.change_password', compact('person'))->render());
        }
    }

    /**
     * Registra una persona con su usuario en la bd
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request) {

        if ($request->ajax()) {

            $this->validate($request, [
                'username' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6',
                'password_confirmation' => 'required|min:6|same:password',
                'name' => 'required',
                'last_name' => 'required',
                'doc_id_type' => 'required|exists:doc_id_types,id',
                'doc_id' => 'required',
                'birthday' => 'required|date|before:tomorrow',
                'gender' => 'required',
                'civil_status' => 'required|exists:civil_statuses,id',
                'phone_type' => 'required|exists:phone_types,id',
                'phone' => 'required|max:13',
                'country' => 'required|exists:countries,id',
                'department' => 'required|exists:departments,id',
                'municipality' => 'required|exists:municipalities,id',
                'address' => 'required'
            ],[
                'password_confirmation.same' => __('messages.pass_confirmation_same'),
                'birthday.before' => __('messages.date_before_tomorrow')
            ]);

            $user = new User();
            $user->name = $request->username;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);

            DB::beginTransaction();
            $user->save();

            $person = new Person();
            $person->user_id = $user->id;
            $person->first_name = $request->name;
            $person->last_name = $request->last_name;
            $person->doc_id_type_id = $request->doc_id_type;
            $person->doc_id = $request->doc_id;
            $person->birthday = $request->birthday;
            $person->gender = $request->gender;
            $person->civil_status_id = $request->civil_status;
            $person->address = $request->address;
            $person->phone_type_id = $request->phone_type;
            $person->phone = $request->phone;

            if (!empty($request->municipality)) {
                $person->municipality_id = $request->municipality;
            }
            $person->save();
            
            DB::commit();

            return response()->json([
                'person' => $person,
            ]);
        }
    }
}
