<?php

namespace App\Http\Controllers;

use App\Email;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

/**
 * Controlador para registro, visualización y envío de emails
 */
class EmailController extends Controller
{
    /**
     * Lista todos los emails registrados
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request) {

        $emails = DB::table('emails AS e')
            ->join('users AS u', 'u.id', '=', 'e.user_id')
            ->join('email_statuses AS es', 'es.id', '=', 'e.email_status_id')
            ->select('e.*', 'u.name AS username', 'es.name AS status')
            ->orderBy('e.created_at', 'DESC')
            ->paginate(10);

        foreach ((object)$emails as $email) {
            if ($email->created_at) {
                $email->created_at = Carbon::parse($email->created_at)->format('d/m/Y h:i:s a');
            }
        }

        return view('emails.index', compact('emails'));
    }

    /**
     * Form para registro de emails
     *
     * @param Request $request
     * @return void
     */
    public function createForm(Request $request) {

        return view('emails.create');
    }

    /**
     * Función para guardar los datos de un email en bd
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request) {

        $this->validate($request, [
            'subject' => 'required',
            'receiver' => 'required|email',
            'message' => 'required',
        ]);

        $email = new Email();
        $email->subject = $request->subject;
        $email->receiver = $request->receiver;
        $email->message = $request->message;
        $email->user_id = Auth::user()->id;
        $email->email_status_id = 1;
        $email->save();

        return redirect('emails')->with([
            'msg_success' => __('messages.changes_saved_successfully'),
            'msg_css' => 'alert-success'
        ]);
    }
}
