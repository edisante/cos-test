<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * Controlador para gestión de autenticación
 */
class AuthController extends Controller
{
    /**
     * Muestra el form de login
     *
     * @return void
     */
    public function login() {
        return view('auth.login');
    }

    /**
     * Inicia la sesión de un usuario
     *
     * @param Request $request
     * @return void
     */
    public function authenticate(Request $request) {

        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:6'
        ],[
            'email.exists' => __('messages.email_not_exists'),
        ]);

        $remember = ($request->has('remember_me')?true:false);

        //Realiza la autenticación y si tiene éxito redirecciona a home
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember)) {
            
            return redirect()->intended('home');
        } else {
            return redirect()->back()->with('error_login', 'Las credenciales ingresadas son incorrectas');
        }
    }

    /**
     * Cierra la sesión de un usuario
     */
    public function logout() {

        Auth::logout();
        return redirect()->intended('/');
    }
}
