<?php

return [
    'unmarried' => 'Soltero(a)',
    'married' => 'Casado(a)',
    'divorced' => 'Divorciado(a)',
    'widowed' => 'Viudo(a)',
    'doc_id_citizen' => 'Cédula de ciudadanía',
    'doc_id_foreign' => 'Cédula de extranjería',
    'passport' => 'Pasaporte',
    'cell_phone' => 'Celular',
    'landline' => 'Fijo',
    'office' => 'Oficina',
    'changes_saved_successfully' => 'Los cambios se han guardado con éxito',
    'not_results' => 'No hay resultados para mostrar',
    'pass_confirmation_same' => 'Las contraseñas deben coincidir',
    'date_before_tomorrow' => 'La fecha no puede ser mayor a la fecha actual',
    'pass_incorrect' => 'Contraseña incorrecta',
    'email_not_exists' => 'Correo electrónico no registrado',
    'not_sended' => 'No enviado',
    'sended' => 'Enviado',
];