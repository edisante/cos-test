@extends('main')

@section('content')
    <div class="container">
        <div class="row align-self-center" >
            <div class="col-12 offset-0 col-md-6 offset-md-3">
                <img src="https://via.placeholder.com/800x150.png?text=LOGO" alt="" class="img-fluid logo-margin-top" style="margin-bottom: 10px;">

                <div class="card">
                    <h5 class="card-header">COS-TEST</h5>
                    <div class="card-body">
                        <h4 class="card-title">@lang('labels.sign_in')</h4>
                        <form action="{{ url('/login') }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="">@lang('labels.email')</label>
                                <input type="email" name="email" id="" class="form-control {{ $errors->has('email')?'is-invalid':'' }}" placeholder="usuario@dominio.com">
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">@lang('labels.password')</label>
                                <input type="password" name="password" id="" class="form-control {{ $errors->has('password')?'is-invalid':'' }}" placeholder="">
                                @if ($errors->has('password'))
                                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-check">
                                      <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" name="remember_me" id="" value="checkedValue" checked>
                                        <small class="text-muted">@lang('labels.remember_session')</small>
                                      </label>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <button class="btn btn-primary" type="submit">@lang('labels.sign_in')</button>
                                    <a href="{{ url('persons') }}" class="btn btn-secondary">@lang('labels.persons_management')</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                {{-- Mensajes de error --}}
                @if (session('error_login'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-top: 10px;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('error_login') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection