<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <div class="container">
        <a href="/home" class="navbar-brand app-logo-margin">
            <img class="img-fluid" src="https://via.placeholder.com/160x50.png?text=LOGO+HEADER" alt="">
        </a>

        <!-- Toggler/collapsibe Button -->
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#collapseButton">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapseButton">
            <ul class="navbar-nav mr-auto">
                @if (Auth::check())
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('emails') }}">@lang('labels.email_management')</a>
                    </li>
                @endif
            </ul>
            <ul class="navbar-nav">
                @if (!Auth::check())
                    <li class="nav-item"><a class="nav-link" href="{{ url('/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;@lang('labels.sign_in')</a></li>
                @else
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            <i class="fa fa-user mr-2" aria-hidden="true"></i>
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu">
                            <a href="{{ url('/logout') }}" class="dropdown-item"><i class="fa fa-sign-out" aria-hidden="true"></i>  @lang('labels.sign_out')</a>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>