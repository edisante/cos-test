@extends('main')

@section('content')
    <div class="container mt-3">
        @if (session('msg_success'))
            <div class="alert {{ session('msg_css') }} alert-dismissible fade show" role="alert">
                {{ session('msg_success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="card">
            <div class="card-header">
                <h5><i class="fa fa-envelope mr-2" aria-hidden="true"></i>@lang('labels.email_management')</h5>
            </div>
            <div class="card-body">
                {{-- Botón de nuevo email --}}
                <a href="{{ url('emails/createForm') }}" class="btn btn-primary">
                    <i class="fa fa-plus mr-2" aria-hidden="true"></i>@lang('labels.new')
                </a>
                <a href="#" class="btn btn-success">
                    @lang('labels.send_emails')
                </a>
                <hr>

                {{-- Listado de emails --}}
                <div class="row">
                    <div class="col-12">
                        @if (!empty($emails) && count($emails) > 0)
                            <div class="table-responsive small">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">
                                                <div class="text-center">
                                                    @lang('labels.subject')
                                                </div>
                                            </th>
                                            <th scope="col">
                                                <div class="text-center">
                                                    @lang('labels.receiver')
                                                </div>
                                            </th>
                                            <th scope="col">
                                                <div class="text-center">
                                                    @lang('labels.username')
                                                </div>
                                            </th>
                                            <th scope="col">
                                                <div class="text-center">
                                                    @lang('labels.email_status')
                                                </div>
                                            </th>
                                            <th scope="col">
                                                <div class="text-center">
                                                    @lang('labels.created_at')
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ((object)$emails as $email)
                                            <tr>
                                                <td scope="row">
                                                    <div class="text-center">
                                                        {{ $email->subject }}
                                                    </div>
                                                </td>
                                                <td scope="row">
                                                    <div class="text-center">
                                                        {{ $email->receiver }}
                                                    </div>
                                                </td>
                                                <td scope="row">
                                                    <div class="text-center">
                                                        {{ $email->username }}
                                                    </div>
                                                </td>
                                                <td scope="row">
                                                    <div class="text-center">
                                                        {{ $email->status }}
                                                    </div>
                                                </td>
                                                <td scope="row">
                                                    <div class="text-center">
                                                        {{ $email->created_at }}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $emails->links() }}
                            </div>
                        @else
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>@lang('messages.not_results')</strong> 
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection