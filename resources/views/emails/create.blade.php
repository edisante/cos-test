@extends('main')

@section('content')
    <div class="container mt-3">
        <div class="card mb-3">
            <div class="card-header">
                <h5><i class="fa fa-envelope mr-2" aria-hidden="true"></i>@lang('labels.new_email')</h5>
            </div>
            <div class="card-body">
                <form action="{{ url('emails/create') }}" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="" class="required">@lang('labels.subject')</label>
                            <input type="text" name="subject" class="form-control {{ $errors->has('subject')?'is-invalid':'' }}" required value="{{ $errors->any()?old('subject'):'' }}">
                            @if ($errors->has('subject'))
                                <div class="invalid-feedback">{{ $errors->first('subject') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="" class="required">@lang('labels.receiver')</label>
                            <input type="email" name="receiver" class="form-control {{ $errors->has('receiver')?'is-invalid':'' }}" required value="{{ $errors->any()?old('receiver'):'' }}">
                            @if ($errors->has('receiver'))
                                <div class="invalid-feedback">{{ $errors->first('receiver') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="" class="required">@lang('labels.message')</label>
                            <textarea name="message" class="form-control {{ $errors->has('message')?'is-invalid':'' }}" id="" cols="30" rows="10" required>{{ $errors->any()?old('message'):'' }}</textarea>
                            @if ($errors->has('message'))
                                <div class="invalid-feedback">{{ $errors->first('message') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12 text-right">
                            <a href="{{ url('emails') }}" class="btn btn-secondary">
                                <i class="fa fa-angle-left mr-2" aria-hidden="true"></i>
                                @lang('labels.back')
                            </a>
                            <button class="btn btn-primary" type="submit">
                                <i class="fa fa-floppy-o mr-2" aria-hidden="true"></i>
                                @lang('labels.save')
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection