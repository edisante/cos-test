@extends('main')

@section('content')
    <div class="container mt-4">
        <div id="containerList">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><i class="fa fa-users mr-2" aria-hidden="true"></i>@lang('labels.persons_management')</h4>
                </div>
                <div class="card-body">
                    <form id="formPersonSearch" action="" method="get">
                        <div class="row">
                            <div class="form-group col-md-6 col-12">
                                <label for="">@lang('labels.username')</label>
                                <input type="text" name="f_username" class="form-control">
                            </div>
                            <div class="form-group col-md-6 col-12">
                                <label for="">@lang('labels.email')</label>
                                <input type="text" name="f_email" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-12">
                                <label for="">@lang('labels.name')</label>
                                <input type="text" name="f_first_name" class="form-control">
                            </div>
                            <div class="form-group col-md-6 col-12">
                                <label for="">@lang('labels.last_name')</label>
                                <input type="text" name="f_last_name" class="form-control">
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-12 text-center">
                                <a href="{{ url('login') }}" class="btn btn-secondary"><i class="fa fa-angle-left mr-2" aria-hidden="true"></i>@lang('labels.go_to_login')</a>
                                <button class="btn btn-primary btn-search" type="button"><i class="fa fa-search mr-2" aria-hidden="true"></i>@lang('labels.search')</button>
                                <button class="btn btn-success btn-new" type="button"><i class="fa fa-file-o mr-2" aria-hidden="true"></i>@lang('labels.new')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div id="containerPersonList" style="display: none"></div>
        </div>
        
        <div id="containerSecondary" style="display: none"></div>
    </div>

    <script>

        $('#containerSecondary').on('click', '.btn-back-change-pass', function() {
            var containerSecondary = $('#containerSecondary');
            var containerList = $('#containerList');

            containerSecondary.hide();
            containerSecondary.empty();
            containerList.show();
        });

        $('#containerPersonList').on('click', '.btn-user-change-password', function() {
            changePass($(this).attr('attr-user-id'));
        });

        function changePass(userId) {
            $('body').loadingModal({text:"@lang('labels.loading')"});

            $.ajax({
                type: "GET",
                url: "{{ url('persons/ajax/changePassword') }}",
                data: {'userId':userId},
                dataType: "json",
                success: function (response) {
                    
                    if (response !== null) {

                        var containerSecondary = $('#containerSecondary');
                        var containerList = $('#containerList');
                        containerSecondary.html(response);

                        containerList.hide();
                        containerSecondary.show();
                    }
                    $('body').loadingModal('destroy');
                },
                error: function (errors) {

                    $('body').loadingModal('destroy');
                }
            });
        };

        $('#containerPersonList').on('click', '.btn-person-delete', function() {
            deletePerson($(this).attr('attr-person-id'));
        });

        function deletePerson(personId) {
            
            $('body').loadingModal({text:"@lang('labels.loading')"});

            $.ajax({
                type: "POST",
                url: "{{ url('persons/ajax/delete') }}",
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                data: {'personId':personId},
                dataType: "json",
                success: function (response) {
                    
                    if (response.success !== null && response.success !== undefined) {

                        getPersonList(1);
                    }
                    $('body').loadingModal('destroy');
                },
                error: function (errors) {

                    $('body').loadingModal('destroy');
                }
            });
        };

        $('#containerPersonList').on('click', '.btn-person-edit', function() {
            updatePerson($(this).attr('attr-person-id'));
        });

        $('#containerSecondary').on('click', '.btn-edit-view-person', function() {
            updatePerson($(this).attr('attr-person-id'));
        });

        $('#containerPersonList').on('click', '.pagination a', function(event) {
            event.preventDefault();

            var page = $(this).attr('href').split('page=')[1];
            getPersonList(page);
        });

        $('#containerSecondary').on('click', '.btn-back-view-person', function() {
            var containerSecondary = $('#containerSecondary');
            var containerList = $('#containerList');

            containerSecondary.hide();
            containerSecondary.empty();
            containerList.show();
        });

        $('#containerPersonList').on('click', '.btn-person-view', function() {

            var personId = $(this).attr('attr-person-id');
            viewPerson(personId);
        });

        function viewPerson(personId) {
            
            $('body').loadingModal({text:"@lang('labels.loading')"});

            $.ajax({
                type: "GET",
                url: "{{ url('persons/ajax/view') }}",
                data: {'personId':personId},
                dataType: "json",
                success: function (response) {
                    
                    if (response != null) {

                        var containerSecondary = $('#containerSecondary');
                        var containerList = $('#containerList');
                        containerSecondary.html(response);

                        containerList.hide();
                        containerSecondary.show();
                    }
                    $('body').loadingModal('destroy');
                },
                error: function (errors) {

                    $('body').loadingModal('destroy');
                }
            });
        };

        $('#formPersonSearch').on('click', '.btn-search', function() {
            getPersonList(1);
        });

        function getPersonList(page) {

            $('body').loadingModal({text:"@lang('labels.loading')"});

            $.ajax({
                type: "GET",
                url: "{{ url('persons/ajax/search?page=') }}"+page,
                data: $('#formPersonSearch').serialize(),
                dataType: "json",
                success: function (response) {
                    
                    if (response != null) {

                        var containerPersonList = $('#containerPersonList');
                        containerPersonList.html(response);

                        containerPersonList.show();
                    }
                    $('body').loadingModal('destroy');
                },
                error: function (errors) {

                    $('body').loadingModal('destroy');
                }
            });
        };

        $('#containerSecondary').on('click', '#formPersonCreate .btn-save-new-person', function() {
            createPerson();
        });

        function updatePerson(personId, saved) {
            
            $('body').loadingModal({text:"@lang('labels.loading')"});

            $.ajax({
                type: "GET",
                url: "{{ url('persons/ajax/updateForm') }}",
                data: {'personId':personId, 'saved':saved},
                dataType: "json",
                success: function (response) {
                    
                    if (response != null) {

                        var containerSecondary = $('#containerSecondary');
                        var containerList = $('#containerList');
                        containerSecondary.html(response);

                        containerList.hide();
                        containerSecondary.show();
                    }
                    $('body').loadingModal('destroy');
                },
                error: function (errors) {

                    $('body').loadingModal('destroy');
                }
            });
        }

        $('#containerSecondary').on('click', '.btn-save-update-person', function() {
            savePerson();
        });

        function savePerson() {
            
            $('body').loadingModal({text:"@lang('labels.loading')"});

            $.ajax({
                type: "POST",
                url: "{{ url('persons/ajax/update') }}",
                data: $('#formPersonCreate').serialize(),
                dataType: "json",
                success: function (response) {
                    
                    $('body').loadingModal('destroy');
                    if (response.person != null && response.person !== undefined) {
                        updatePerson(response.person.id, true);
                    }
                },
                error: function (errors) {

                    showValidationErros(errors);
                    
                    $('body').loadingModal('destroy');
                }
            });
        };

        function createPerson() {
        
            $('body').loadingModal({text:"@lang('labels.loading')"});

            $.ajax({
                type: "POST",
                url: "{{ url('persons/ajax/create') }}",
                data: $('#formPersonCreate').serialize(),
                dataType: "json",
                success: function (response) {
                    
                    $('body').loadingModal('destroy');
                    if (response.person != null && response.person !== undefined) {
                        updatePerson(response.person.id, true);
                    }
                },
                error: function (errors) {

                    showValidationErros(errors);
                    
                    $('body').loadingModal('destroy');
                }
            });
        };

        function showValidationErros(errors) {
            
            $('#formPersonCreate').find('.form-control').removeClass('is-invalid');
            $('#formPersonCreate').find('.invalid-feedback').empty();

            if (errors.responseJSON.errors.username) {
                $('#formPersonCreate').find('input[name="username"]').addClass('is-invalid');
                $('#formPersonCreate').find('input[name="username"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.username[0]);
            }
            if (errors.responseJSON.errors.email) {
                $('#formPersonCreate').find('input[name="email"]').addClass('is-invalid');
                $('#formPersonCreate').find('input[name="email"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.email[0]);
            }
            if (errors.responseJSON.errors.password) {
                $('#formPersonCreate').find('input[name="password"]').addClass('is-invalid');
                $('#formPersonCreate').find('input[name="password"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.password[0]);
            }
            if (errors.responseJSON.errors.password_confirmation) {
                $('#formPersonCreate').find('input[name="password_confirmation"]').addClass('is-invalid');
                $('#formPersonCreate').find('input[name="password_confirmation"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.password_confirmation[0]);
            }
            if (errors.responseJSON.errors.name) {
                $('#formPersonCreate').find('input[name="name"]').addClass('is-invalid');
                $('#formPersonCreate').find('input[name="name"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.name[0]);
            }
            if (errors.responseJSON.errors.last_name) {
                $('#formPersonCreate').find('input[name="last_name"]').addClass('is-invalid');
                $('#formPersonCreate').find('input[name="last_name"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.last_name[0]);
            }
            if (errors.responseJSON.errors.doc_id_type) {
                $('#formPersonCreate').find('select[name="doc_id_type"]').addClass('is-invalid');
                $('#formPersonCreate').find('select[name="doc_id_type"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.doc_id_type[0]);
            }
            if (errors.responseJSON.errors.doc_id) {
                $('#formPersonCreate').find('input[name="doc_id"]').addClass('is-invalid');
                $('#formPersonCreate').find('input[name="doc_id"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.doc_id[0]);
            }
            if (errors.responseJSON.errors.birthday) {
                $('#formPersonCreate').find('input[name="birthday"]').addClass('is-invalid');
                $('#formPersonCreate').find('input[name="birthday"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.birthday[0]);
            }
            if (errors.responseJSON.errors.gender) {
                $('#formPersonCreate').find('input[name="gender"]').addClass('is-invalid');
            }
            if (errors.responseJSON.errors.civil_status) {
                $('#formPersonCreate').find('select[name="civil_status"]').addClass('is-invalid');
                $('#formPersonCreate').find('select[name="civil_status"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.civil_status[0]);
            }
            if (errors.responseJSON.errors.phone_type) {
                $('#formPersonCreate').find('select[name="phone_type"]').addClass('is-invalid');
                $('#formPersonCreate').find('select[name="phone_type"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.phone_type[0]);
            }
            if (errors.responseJSON.errors.phone) {
                $('#formPersonCreate').find('input[name="phone"]').addClass('is-invalid');
                $('#formPersonCreate').find('input[name="phone"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.phone[0]);
            }
            if (errors.responseJSON.errors.country) {
                $('#formPersonCreate').find('select[name="country"]').addClass('is-invalid');
                $('#formPersonCreate').find('select[name="country"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.country[0]);
            }
            if (errors.responseJSON.errors.department) {
                $('#formPersonCreate').find('select[name="department"]').addClass('is-invalid');
                $('#formPersonCreate').find('select[name="department"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.department[0]);
            }
            if (errors.responseJSON.errors.municipality) {
                $('#formPersonCreate').find('select[name="municipality"]').addClass('is-invalid');
                $('#formPersonCreate').find('select[name="municipality"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.municipality[0]);
            }
            if (errors.responseJSON.errors.address) {
                $('#formPersonCreate').find('input[name="address"]').addClass('is-invalid');
                $('#formPersonCreate').find('input[name="address"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.address[0]);
            }
        };

        $('#containerList').on('click', '.btn-new', function () {
           
            $('body').loadingModal({text:"@lang('labels.loading')"});

            $.ajax({
                type: "GET",
                url: "{{ url('persons/ajax/createForm') }}",
                data: {},
                dataType: "json",
                success: function (response) {
                    
                    if (response != null) {

                        var containerSecondary = $('#containerSecondary');
                        var containerList = $('#containerList');
                        containerSecondary.html(response);

                        containerList.hide();
                        containerSecondary.show();
                    }
                    $('body').loadingModal('destroy');
                },
                error: function (errors) {

                    $('body').loadingModal('destroy');
                }
            });
        });

        $('#containerSecondary').on('click', '.btn-back-new-person', function() {
            var containerSecondary = $('#containerSecondary');
            var containerList = $('#containerList');

            containerSecondary.hide();
            containerSecondary.empty();
            getPersonList(1);
            containerList.show();
        });

    </script>
    
@endsection