<div class="card mb-4">
    <div class="card-header">
        <h4 class="card-title"><i class="fa fa-user-o mr-2" aria-hidden="true"></i>@lang('labels.person_details')</h4>
    </div>
    <div class="card-body">
        <form>
            <div class="row">
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.username')</label>
                <input type="text" name="username" class="form-control" value="{{ isset($person->username)?$person->username:'' }}" readonly>
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.email')</label>
                    <input type="email" name="email" class="form-control" value="{{ isset($person->email)?$person->email:'' }}" readonly>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.name')</label>
                    <input type="text" name="name" class="form-control" value="{{ isset($person->first_name)?$person->first_name:'' }}" readonly>
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.last_name')</label>
                    <input type="text" name="last_name" class="form-control" value="{{ isset($person->last_name)?$person->last_name:'' }}" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3 col-12 col-sm-12">
                    <label for="" class="required">@lang('labels.doc_id_type')</label>
                    <input type="text" class="form-control" value="{{ isset($doc_id_types->name)?$doc_id_types->name:'' }}" readonly>
                </div>
                <div class="form-group col-md-3 col-12 col-sm-12">
                    <label for="" class="required">@lang('labels.doc_number')</label>
                    <input type="text" name="doc_id" class="form-control" value="{{ isset($person->doc_id)?$person->doc_id:'' }}" readonly>
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.birthday')</label>
                    <input type="date" name="birthday" class="form-control" value="{{ isset($person->birthday)?$person->birthday:'' }}" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.gender')</label>
                    <div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="gender" id="genderMale" value="1" {{ isset($person->gender) && $person->gender == '1' ? 'checked' : ''}} disabled>
                            <label class="form-check-label" for="genderMale">@lang('labels.male')</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="gender" id="genderFemale" value="2" {{ isset($person->gender) && $person->gender == '2' ? 'checked' : ''}} disabled>
                            <label class="form-check-label" for="genderFemale">@lang('labels.female')</label>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.civil_status')</label>
                    <input type="text" class="form-control" value="{{ isset($civil_statuses->name)?$civil_statuses->name:'' }}" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3 col-12 col-sm-12">
                    <label for="" class="required">@lang('labels.phone')</label>
                    <input type="text" class="form-control" value="{{ isset($phone_types->name)?$phone_types->name:'' }}" readonly>
                </div>
                <div class="form-group col-md-3 col-12 col-sm-12">
                    <label for="" class="required">@lang('labels.number')</label>
                    <input type="text" name="phone" maxlength="13" class="form-control" value="{{ isset($person->phone)?$person->phone:'' }}" readonly>
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.country')</label>
                    <input type="text" class="form-control" value="{{ isset($country->name)?$country->name:'' }}" readonly>
                </div>
            </div>
            
            <div class="row">
                @isset($department)
                    <div class="form-group col-md-6 col-12 dep-content">
                        <label for="" class="required">@lang('labels.department')</label>
                        <input type="text" class="form-control" value="{{ isset($department->name)?$department->name:'' }}" readonly>
                    </div>
                @endisset
                
                @isset($municipality)
                    <div class="form-group col-md-6 col-12 mun-content">
                        <label for="" class="required">@lang('labels.municipality')</label>
                        <input type="text" class="form-control" value="{{ isset($municipality->name)?$municipality->name:'' }}" readonly>
                    </div>
                @endisset
            </div>
        
            <div class="row">
                <div class="form-group col-md-12 col-12">
                    <label for="" class="required">@lang('labels.address')</label>
                    <input type="text" name="address" class="form-control" value="{{ isset($person->address)?$person->address:'' }}" readonly>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 text-center">
                    <button class="btn btn-secondary btn-back-view-person" type="button"><i class="fa fa-angle-left mr-2" aria-hidden="true"></i>@lang('labels.back')</button>
                    <button class="btn btn-primary btn-edit-view-person" attr-person-id="{{ $person->id }}" type="button"><i class="fa fa-pencil-square-o mr-2" aria-hidden="true"></i>@lang('labels.edit')</button>
                </div>
            </div>
        </form>
    </div>
</div>
