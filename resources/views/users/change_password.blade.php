<div id="containerNotifications"></div>
<div class="card">
    <div class="card-header">
        <h4 class="card-title"><i class="fa fa-lock mr-2" aria-hidden="true"></i>@lang('labels.change_pass')</h4>
    </div>
    <div class="card-body">
        <form id="formChangePass" action="" method="post">
            {{ csrf_field() }}
            @isset($person->user_id)
                <input type="hidden" name="user_id" value="{{ $person->user_id }}">
            @endisset
            <div class="row">
                <div class="form-group offset-md-2 col-md-8 col-12">
                    <label for="">@lang('labels.username')</label>
                    <input type="text" class="form-control" value="{{ isset($person)?$person->first_name.' '.$person->last_name:'' }}" disabled>
                </div>
                <div class="form-group offset-md-2 col-md-8 col-12">
                    <label for="" class="required">@lang('labels.current_pass')</label>
                    <input type="password" name="current_pass" class="form-control">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group offset-md-2 col-md-8 col-12">
                    <label for="" class="required">@lang('labels.new_pass')</label>
                    <input type="password" name="new_pass" class="form-control">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group offset-md-2 col-md-8 col-12">
                    <label for="" class="required">@lang('labels.password_confirmation')</label>
                    <input type="password" name="password_confirmation" class="form-control">
                    <div class="invalid-feedback"></div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 text-center">
                    <button class="btn btn-secondary btn-back-change-pass" type="button">
                        <i class="fa fa-angle-left mr-2" aria-hidden="true"></i>
                        @lang('labels.back')
                    </button>
                    <button class="btn btn-primary btn-save-change-pass" type="button">
                        <i class="fa fa-floppy-o mr-2" aria-hidden="true"></i>
                        @lang('labels.save')
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    $('#formChangePass').on('click', '.btn-save-change-pass', function() {
        savePass();
    });

    function savePass() {
        $('body').loadingModal({text:"@lang('labels.loading')"});

        $.ajax({
            type: "POST",
            url: "{{ url('persons/ajax/savePass') }}",
            data: $('#formChangePass').serialize(),
            dataType: "json",
            success: function (response) {
                
                if (response.success !== null && response.success !== undefined) {
                    $('#formChangePass').find('.form-control').removeClass('is-invalid');
                    $('#formChangePass').find('.invalid-feedback').empty();

                    $('#containerNotifications').append(''+
                        '<div class="alert alert-success alert-dismissible fade show" role="alert">'+
                            "@lang('messages.changes_saved_successfully')"+
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                '<span aria-hidden="true">&times;</span>'+
                            '</button>'+
                        '</div>'
                    );
                }
                $('body').loadingModal('destroy');
            },
            error: function (errors) {

                $('#formChangePass').find('.form-control').removeClass('is-invalid');
                $('#formChangePass').find('.invalid-feedback').empty();

                if (errors.responseJSON.errors.current_pass) {
                    $('#formChangePass').find('input[name="current_pass"]').addClass('is-invalid');
                    $('#formChangePass').find('input[name="current_pass"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.current_pass[0]);
                }
                if (errors.responseJSON.errors.new_pass) {
                    $('#formChangePass').find('input[name="new_pass"]').addClass('is-invalid');
                    $('#formChangePass').find('input[name="new_pass"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.new_pass[0]);
                }
                if (errors.responseJSON.errors.password_confirmation) {
                    $('#formChangePass').find('input[name="password_confirmation"]').addClass('is-invalid');
                    $('#formChangePass').find('input[name="password_confirmation"]').closest('div').find('.invalid-feedback').text(errors.responseJSON.errors.password_confirmation[0]);
                }

                $('body').loadingModal('destroy');
            }
        });
    };
</script>