<div class="card mt-3">
    <div class="card-header">
        <h6 class="card-title text-center">@lang('labels.people_list')</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                @if (!empty($persons) && count($persons) > 0)
                    <div class="table-responsive small">
                        <table id="tbPersonList" class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">
                                        <div class="text-center"></div>
                                    </th>
                                    <th scope="col">
                                        <div class="text-center">@lang('labels.username')</div>
                                    </th>
                                    <th scope="col">
                                        <div class="text-center">@lang('labels.email')</div>
                                    </th>
                                    <th scope="col">
                                        <div class="text-center">@lang('labels.name')</div>
                                    </th>
                                    <th scope="col">
                                        <div class="text-left">@lang('labels.doc_id')</div>
                                    </th>
                                    <th scope="col">
                                        <div class="text-center">@lang('labels.phone')</div>
                                    </th>
                                    <th scope="col">
                                        <div class="text-center">@lang('labels.birthday')</div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ((object)$persons as $person)
                                    <tr>
                                        <td scope="row">
                                            <div class="text-center no-wrap-text">
                                                <button class="btn btn-sm app-btn-sm btn-outline-secondary btn-person-view" attr-person-id="{{ $person->id }}" data-toggle="tooltip" title="@lang('labels.view')">
                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-sm app-btn-sm btn-outline-secondary btn-person-edit" attr-person-id="{{ $person->id }}" data-toggle="tooltip" title="@lang('labels.edit')">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-sm app-btn-sm btn-outline-secondary btn-user-change-password" attr-user-id="{{ $person->user_id }}" data-toggle="tooltip" title="@lang('labels.change_pass')">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-sm app-btn-sm btn-outline-danger btn-person-delete" attr-person-id="{{ $person->id }}" data-toggle="tooltip" title="@lang('labels.delete')">
                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </td>
                                        <td scope="row">
                                            <div class="text-center">{{ $person->username }}</div>
                                        </td>
                                        <td scope="row">
                                            <div class="text-center">{{ $person->email }}</div>
                                        </td>
                                        <td scope="row">
                                            <div class="text-center">{{ $person->first_name." ".$person->last_name }}</div>
                                        </td>
                                        <td scope="row">
                                            <div class="text-left">
                                                {{ $person->doc_id_type.": ".$person->doc_id }}
                                            </div>
                                        </td>
                                        <td scope="row">
                                            <div class="text-center">
                                                {{ $person->phone_type.": ".$person->phone }}
                                            </div>
                                        </td>
                                        <td scope="row">
                                            <div class="text-center">{{ $person->birthday }}</div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $persons->links() }}
                    </div>
                @else
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>@lang('messages.not_results')</strong> 
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>