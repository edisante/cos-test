@if (isset($saved) && $saved)
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        @lang('messages.changes_saved_successfully')
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<div class="card mb-4">
    <div class="card-header">
        @if (isset($person->id))
            <h4 class="card-title"><i class="fa fa-pencil-square-o mr-2" aria-hidden="true"></i>@lang('labels.update_person')</h4>
        @else
            <h4 class="card-title"><i class="fa fa-plus mr-2" aria-hidden="true"></i>@lang('labels.new_person')</h4>
        @endif
    </div>
    <div class="card-body">
        <form id="formPersonCreate" action="{{ url('persons/ajax/create') }}" method="post">
            {{ csrf_field() }}
            @isset($person->user_id)
                <input type="hidden" name="user_id" value="{{ $person->user_id }}">
            @endisset
            <div class="row">
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.username')</label>
                    <input type="text" name="username" class="form-control" value="{{ isset($person->username)?$person->username:'' }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.email')</label>
                    <input type="email" name="email" class="form-control" value="{{ isset($person->email)?$person->email:'' }}">
                    <div class="invalid-feedback"></div>
                </div>
            </div>

            @if (empty($person->id))
                <div class="row">
                    <div class="form-group col-md-6 col-12">
                        <label for="" class="required">@lang('labels.password')</label>
                        <input type="password" name="password" class="form-control">
                        <div class="invalid-feedback"></div>
                    </div>
                    <div class="form-group col-md-6 col-12">
                        <label for="" class="required">@lang('labels.password_confirmation')</label>
                        <input type="password" name="password_confirmation" class="form-control">
                        <div class="invalid-feedback"></div>
                    </div>
                </div>
            @endif
            <hr>

            <div class="row">
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.name')</label>
                    <input type="text" name="name" class="form-control" value="{{ isset($person->first_name)?$person->first_name:'' }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.last_name')</label>
                    <input type="text" name="last_name" class="form-control" value="{{ isset($person->last_name)?$person->last_name:'' }}">
                    <div class="invalid-feedback"></div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3 col-12 col-sm-12">
                    <label for="" class="required">@lang('labels.doc_id_type')</label>
                    <select name="doc_id_type" class="form-control">
                        <option value="">@lang('labels.select_option')</option>
                        @isset($doc_id_types)
                            @foreach ($doc_id_types as $doc_id_type)
                                <option value="{{ $doc_id_type->id }}" {{ $doc_id_type->selected }}>{{ $doc_id_type->name }}</option>
                            @endforeach
                        @endisset
                    </select>
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group col-md-3 col-12 col-sm-12">
                    <label for="" class="required">@lang('labels.doc_number')</label>
                    <input type="text" name="doc_id" class="form-control" value="{{ isset($person->doc_id)?$person->doc_id:'' }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.birthday')</label>
                    <input type="date" name="birthday" class="form-control" value="{{ isset($person->birthday)?$person->birthday:'' }}">
                    <div class="invalid-feedback"></div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.gender')</label>
                    <div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="gender" id="genderMale" value="1" {{ isset($person->gender) && $person->gender == '1' ? 'checked' : ''}}>
                            <label class="form-check-label" for="genderMale">@lang('labels.male')</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="gender" id="genderFemale" value="2" {{ isset($person->gender) && $person->gender == '2' ? 'checked' : ''}}>
                            <label class="form-check-label" for="genderFemale">@lang('labels.female')</label>
                        </div>
                    </div>
                    <div class="gender invalid-feedback"></div>
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.civil_status')</label>
                    <select name="civil_status" class="form-control">
                        <option value="">@lang('labels.select_option')</option>
                        @isset($civil_statuses)
                            @foreach ($civil_statuses as $civil_status)
                                <option value="{{ $civil_status->id }}" {{ $civil_status->selected }}>{{ $civil_status->name }}</option>
                            @endforeach
                        @endisset
                    </select>
                    <div class="invalid-feedback"></div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3 col-12 col-sm-12">
                    <label for="" class="required">@lang('labels.phone')</label>
                    <select name="phone_type" class="form-control">
                        <option value="">@lang('labels.select_option')</option>
                        @isset($phone_types)
                            @foreach ($phone_types as $phone_type)
                                <option value="{{ $phone_type->id }}" {{ $phone_type->selected }}>{{ $phone_type->name }}</option>
                            @endforeach
                        @endisset
                    </select>
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group col-md-3 col-12 col-sm-12">
                    <label for="" class="required">@lang('labels.number')</label>
                    <input type="text" name="phone" maxlength="13" class="form-control" value="{{ isset($person->phone)?$person->phone:'' }}">
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group col-md-6 col-12">
                    <label for="" class="required">@lang('labels.country')</label>
                    <select id="selectCountries" name="country" class="form-control">
                        <option value="">@lang('labels.select_option')</option>
                        @isset($countries)
                            @foreach ($countries as $country)
                                <option value="{{ $country->id }}" {{ $country->selected }}>{{ $country->name }}</option>
                            @endforeach
                        @endisset
                    </select>
                    <div class="invalid-feedback"></div>
                </div>
            </div>
            <div id="containerDepartments" style="{{ !empty($person->municipality_id) ? '' : 'display: none;' }}">
                <div class="row">
                    <div class="form-group col-md-6 col-12 dep-content">
                        <label for="" class="required">@lang('labels.department')</label>
                        <select id="selectDepartments" name="department" class="form-control">
                            <option value="">@lang('labels.select_option')</option>
                            @isset($departments)
                                @foreach ($departments as $department)
                                    <option value="{{ $department->id }}" {{ $department->selected }}>{{ $department->name }}</option>
                                @endforeach
                            @endisset
                        </select>
                    <div class="invalid-feedback"></div>
                    </div>

                    <div class="form-group col-md-6 col-12 mun-content">
                        <label for="" class="required">@lang('labels.municipality')</label>
                        <select id="selectMunicipalities" name="municipality" class="form-control">
                            <option value="">@lang('labels.select_option')</option>
                            @isset($municipalities)
                                @foreach ($municipalities as $municipality)
                                    <option value="{{ $municipality->id }}" {{ $municipality->selected }}>{{ $municipality->name }}</option>
                                @endforeach
                            @endisset
                        </select>
                    <div class="invalid-feedback"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12 col-12">
                    <label for="" class="required">@lang('labels.address')</label>
                    <input type="text" name="address" class="form-control" value="{{ isset($person->address)?$person->address:'' }}">
                    <div class="invalid-feedback"></div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 text-center">
                    <button class="btn btn-secondary btn-back-new-person" type="button"><i class="fa fa-angle-left mr-2" aria-hidden="true"></i>@lang('labels.back')</button>
                    @if (!empty($person->id))
                        <button class="btn btn-primary btn-save-update-person" type="button"><i class="fa fa-floppy-o mr-2" aria-hidden="true"></i>@lang('labels.save')</button>
                    @else
                        <button class="btn btn-primary btn-save-new-person" type="button"><i class="fa fa-floppy-o mr-2" aria-hidden="true"></i>@lang('labels.save')</button>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    
    $('#selectCountries').on('change', function() {

        if ($(this).val() == "{{ config('cos.ColombiaCountryId') }}") {
            getDepartmentsByCountry($(this).val());
            $('#containerDepartments').show();
        } else {
            $('#selectDepartments').empty();
            $('#selectDepartments').append(''+
                '<option value="">@lang("labels.select_option")</option>'
            );
            $('#selectMunicipalities').empty();
            $('#selectMunicipalities').append(''+
                '<option value="">@lang("labels.select_option")</option>'
            );
            $('#containerDepartments').hide();
        }

    });

    $('#selectDepartments').on('change', function() {

        if ($(this).val() != '') {
            getMunicipalitiesByDepartment($(this).val());
        } else {
            $('#selectMunicipalities').empty();
            $('#selectMunicipalities').append(''+
                '<option value="">@lang("labels.select_option")</option>'
            );
        }
    });

    function getMunicipalitiesByDepartment(departmentId) {
        
        $('body').loadingModal({text:"@lang('labels.loading')"});

        $.ajax({
            type: "GET",
            url: "{{ url('persons/ajax/getMunicipalitiesByDepartment') }}",
            data: {'departmentId':departmentId},
            dataType: "json",
            success: function (response) {
                
                if (response != null) {

                    $('#selectMunicipalities').empty();
                    $('#selectMunicipalities').append(response);
                }
                $('body').loadingModal('destroy');
            },
            error: function (errors) {

                $('body').loadingModal('destroy');
            }
        });
    }

    function getDepartmentsByCountry(countryId) {
            
            $('body').loadingModal({text:"@lang('labels.loading')"});

            $.ajax({
                type: "GET",
                url: "{{ url('persons/ajax/getDepartmentsByCountry') }}",
                data: {'countryId':countryId},
                dataType: "json",
                success: function (response) {
                    
                    if (response != null) {

                        $('#selectDepartments').empty();
                        $('#selectDepartments').append(response);
                    }
                    $('body').loadingModal('destroy');
                },
                error: function (errors) {

                    $('body').loadingModal('destroy');
                }
            });
        }
</script>