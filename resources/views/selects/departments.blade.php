<option value="">@lang('labels.select_option')</option>
@isset($departments)
    @foreach ($departments as $department)
        <option value="{{ $department->id }}">{{ $department->name }}</option>
    @endforeach
@endisset