<option value="">@lang('labels.select_option')</option>
@isset($municipalities)
    @foreach ($municipalities as $municipality)
        <option value="{{ $municipality->id }}">{{ $municipality->name }}</option>
    @endforeach
@endisset