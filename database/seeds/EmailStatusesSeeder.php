<?php

use Illuminate\Database\Seeder;

class EmailStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('email_statuses')->insert([
            ['id' => 1, 'name' => __('messages.not_sended')],
            ['id' => 2, 'name' => __('messages.sended')],
        ]);
    }
}
