<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DocIdTypesSeeder::class);
        $this->call(PhoneTypesSeeder::class);
        $this->call(CivilStatusSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(DepartmentsSeeder::class);
        $this->call(MunicipalitiesSeeder::class);
        $this->call(EmailStatusesSeeder::class);
    }
}
