<?php

use Illuminate\Database\Seeder;

class PhoneTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phone_types')->insert([
            ['name' => __('messages.cell_phone')],
            ['name' => __('messages.landline')],
            ['name' => __('messages.office')],
        ]);
    }
}
