<?php

use Illuminate\Database\Seeder;

class DocIdTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doc_id_types')->insert([
            ['name' => __('messages.doc_id_citizen')],
            ['name' => __('messages.doc_id_foreign')],
            ['name' => __('messages.passport')],
        ]);
    }
}
