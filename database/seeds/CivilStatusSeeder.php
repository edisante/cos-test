<?php

use Illuminate\Database\Seeder;

class CivilStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('civil_statuses')->insert([
            ['name' => __('messages.unmarried')],
            ['name' => __('messages.married')],
            ['name' => __('messages.divorced')],
            ['name' => __('messages.widowed')],
        ]);
    }
}
