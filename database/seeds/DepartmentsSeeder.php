<?php

use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            ['id' => 4, 'name' => 'Bogotá, D.C.', 'country_id' => 82],
            ['id' => 30, 'name' => 'Amazonas', 'country_id' => 82],
            ['id' => 2, 'name' => 'Antioquia', 'country_id' => 82],
            ['id' => 26, 'name' => 'Arauca', 'country_id' => 82],
            ['id' => 29, 'name' => 'Archipiélago de San Andrés, Providencia y Santa Catalina', 'country_id' => 82],
            ['id' => 3, 'name' => 'Atlántico', 'country_id' => 82],
            ['id' => 5, 'name' => 'Bolívar', 'country_id' => 82],
            ['id' => 6, 'name' => 'Boyacá', 'country_id' => 82],
            ['id' => 7, 'name' => 'Caldas', 'country_id' => 82],
            ['id' => 8, 'name' => 'Caquetá', 'country_id' => 82],
            ['id' => 27, 'name' => 'Casanare', 'country_id' => 82],
            ['id' => 9, 'name' => 'Cauca', 'country_id' => 82],
            ['id' => 10, 'name' => 'Cesar', 'country_id' => 82],
            ['id' => 13, 'name' => 'Chocó', 'country_id' => 82],
            ['id' => 11, 'name' => 'Córdoba', 'country_id' => 82],
            ['id' => 12, 'name' => 'Cundinamarca', 'country_id' => 82],
            ['id' => 31, 'name' => 'Guainía', 'country_id' => 82],
            ['id' => 32, 'name' => 'Guaviare', 'country_id' => 82],
            ['id' => 14, 'name' => 'Huila', 'country_id' => 82],
            ['id' => 15, 'name' => 'La Guajira', 'country_id' => 82],
            ['id' => 16, 'name' => 'Magdalena', 'country_id' => 82],
            ['id' => 17, 'name' => 'Meta', 'country_id' => 82],
            ['id' => 18, 'name' => 'Nariño', 'country_id' => 82],
            ['id' => 19, 'name' => 'Norte de Santander', 'country_id' => 82],
            ['id' => 28, 'name' => 'Putumayo', 'country_id' => 82],
            ['id' => 20, 'name' => 'Quindio', 'country_id' => 82],
            ['id' => 21, 'name' => 'Risaralda', 'country_id' => 82],
            ['id' => 22, 'name' => 'Santander', 'country_id' => 82],
            ['id' => 23, 'name' => 'Sucre', 'country_id' => 82],
            ['id' => 24, 'name' => 'Tolima', 'country_id' => 82],
            ['id' => 25, 'name' => 'Valle del Cauca', 'country_id' => 82],
            ['id' => 33, 'name' => 'Vaupés', 'country_id' => 82],
            ['id' => 34, 'name' => 'Vichada', 'country_id' => 82],
        ]);
    }
}
