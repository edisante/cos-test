<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocIdTypesTable extends Migration
{
    /**
     * Run the migrations.
     * Tipos de documentos de identidad
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doc_id_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 60);
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doc_id_types');
    }
}
