<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * Tabla de persona
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned(); //Id de usuario
            $table->string('first_name');   //Nombre
            $table->string('last_name');    //Apellido
            $table->integer('doc_id_type_id')->unsigned();  //Tipo de documento
            $table->string('doc_id', 50);   //Número del documento
            $table->date('birthday');   //Fecha de nacimiento
            $table->tinyInteger('gender');  //Género
            $table->integer('civil_status_id')->unsigned(); //Estado civíl
            $table->integer('phone_type_id')->unsigned();   //Tipo de teléfono
            $table->string('phone', 20);    //Número de teléfono
            $table->integer('municipality_id')->unsigned()->nullable(); //Municipio
            $table->string('address');  //Dirección
            $table->text('photo')->nullable();  //Foto
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('restrict')->onDelete('restrict');

            $table->foreign('doc_id_type_id')->references('id')->on('doc_id_types')
                ->onUpdate('restrict')->onDelete('restrict');

            $table->foreign('civil_status_id')->references('id')->on('civil_statuses')
                ->onUpdate('restrict')->onDelete('restrict');

            $table->foreign('municipality_id')->references('id')->on('municipalities')
                ->onUpdate('restrict')->onDelete('restrict');

            $table->foreign('phone_type_id')->references('id')->on('phone_types')
                ->onUpdate('restrict')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
